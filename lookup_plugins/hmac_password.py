import base64
import hashlib
import hmac

from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        data = ':'.join(terms).encode('ascii')
        key = variables['hmac_password_key'].encode('ascii')
        return [base64.b64encode(hmac.new(key, data, hashlib.sha256).digest()).decode('UTF-8').strip('=')]
